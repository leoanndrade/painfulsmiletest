using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private float followSpeed;

    private Transform followTarget;
    private Transform myTransform;

    public Transform SetTarget
    {
        set
        {
            myTransform = this.transform;
            followTarget = value;
        }
    }

    private void LateUpdate()
    {
        if (followTarget != null)
        {
            myTransform.position = Vector3.Lerp(myTransform.position,
                                                new Vector3(followTarget.position.x, followTarget.position.y, myTransform.position.z),
                                                followSpeed * Time.deltaTime);
        }
    }
}