using System.Collections;
using UnityEngine;

[System.Serializable]
public struct HealthStruct
{
    [HideInInspector]
    public GameObject obj;
    [HideInInspector]
    public float maxHealth;
    public float health;
    [HideInInspector]
    public bool isPlayer;
}

public class DamageableObject : MonoBehaviour
{
    [Header("--------- DAMAGEABLE OBJECT ---------")]
    [SerializeField] protected HealthStruct healthStruct;
    [SerializeField] protected AnimationClip deathAnimClip;

    [Header("VFX")]
    [SerializeField] private GameObject objExplosionBig;
    [SerializeField] private GameObject objExplosionSmall;
    [SerializeField] private GameObject objFire;

    private bool isOnFire = false;

    protected void DamageablObjectStartCustom()
    {
        healthStruct.maxHealth = healthStruct.health;

        healthStruct.obj = this.gameObject;
        
        EventManager.HealthChange(healthStruct);

        EventManager.DamageableObjInstantiate(this);
    }

    public void TakeDamage(float damage, DamageableObject damageableObject = null)
    {
        if (healthStruct.health <= 0)
            return;

        healthStruct.health -= damage;

        EventManager.HealthChange(healthStruct);

        if (healthStruct.health <= 0)
        {
            if (damageableObject != null && healthStruct.obj != null)
                if (!healthStruct.obj.CompareTag("Player") && damageableObject.CompareTag("Player"))
                    EventManager.GivePlayerPoint(1);

            StartCoroutine(Destroyed());
        }
        else
        {
            Instantiate(objExplosionSmall, healthStruct.obj.transform.position, Quaternion.identity);

            if (healthStruct.health < healthStruct.maxHealth / 4 && !isOnFire)
            {
                isOnFire = true;
                Instantiate(objFire, healthStruct.obj.transform);
            }
        }
    }

    private IEnumerator Destroyed()
    {
        Instantiate(objExplosionBig, healthStruct.obj.transform.position, Quaternion.identity);

        Animation anim = this.GetComponent<Animation>();

        if (anim != null && deathAnimClip != null)
        {
            yield return new WaitForSeconds(2);

            Instantiate(objExplosionBig, healthStruct.obj.transform.position, Quaternion.identity);

            anim.clip = deathAnimClip;
            anim.Play();

            while (anim.isPlaying)
                yield return null;
        }

        Destroy(this.gameObject);
    }

    public float GetHealth
    {
        get
        {
            return healthStruct.health;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Terrain"))
        {
            TakeDamage(10);
        }
        else if (collision.gameObject.CompareTag("Ship") || collision.gameObject.CompareTag("Player"))
        {
            TakeDamage(5);
        }
    }
}