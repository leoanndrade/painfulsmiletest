using UnityEngine;

public class ParticleAutoDestroyer : MonoBehaviour
{
    private ParticleSystem ps;

    private void Start()
    {
        ps = this.GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        if (ps != null && !ps.isEmitting)
        {
            Destroy(this.gameObject);
        }
    }
}