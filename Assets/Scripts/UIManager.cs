using UnityEngine;

public class UIManager : Window
{
    private void Start()
    {
        StartCustom();
    }

    protected override void StartCustom()
    {
        base.StartCustom();
        StopAllCoroutines();
        StartCoroutine(FadeTransition.Fade(FadeTransition.FadeType.IN));
    }

    public void PlayButton()
    {
        GameManager.ChangeScene(GameManager.Scenes.GAME_SCENE);
    }

   
}