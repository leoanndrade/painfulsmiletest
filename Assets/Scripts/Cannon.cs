using UnityEngine;

[System.Serializable]
public struct CannonConfig
{
    public GameObject projectileObj;
    public float fireRate;
    public float projectileSpeed;
    public Transform spawnPoint;
    public SpriteRenderer mySpriteRenderer;
    public Sprite[] sprites;
}

[System.Serializable]
public class Cannon : MonoBehaviour
{
    [SerializeField] private CannonConfig config;
    [SerializeField] private Cannon[] cannonChildArray;

    private GameObject rootObj;
    private Transform myTransform;

    private float fireCoolDown = 0;

    private void Awake()
    {
        EventManager.OnHealthChange += HealthChange;
    }

    private void Start()
    {
        myTransform = this.transform;
        rootObj = myTransform.root.gameObject;
    }

    private void Update()
    {
        fireCoolDown += Time.deltaTime;
    }

    private void HealthChange(HealthStruct healthStruct)
    {
        if (rootObj != healthStruct.obj)
            return;

        config.mySpriteRenderer.sprite = config.sprites[healthStruct.health != 0 ? 0 : 1];
    }

    public void Shoot()
    {
        if (fireCoolDown < config.fireRate)
            return;

        fireCoolDown = 0;

        Projectile projectile =  Instantiate(config.projectileObj,
                                 config.spawnPoint.position,
                                 Quaternion.identity).GetComponent<Projectile>();

        if (projectile != null)
            projectile.Initialize(rootObj, -myTransform.up, config.projectileSpeed);
        else
        {
            Debug.LogError("'Projectile' class not found");
            return;
        }

        if (cannonChildArray is null)
            return;

        for (int i = 0; i < cannonChildArray.Length; i++) 
        {
            cannonChildArray[i].Shoot();
        }
    }
}