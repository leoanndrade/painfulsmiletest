using UnityEngine;

public class Window : MonoBehaviour
{
    private WindowCustom[] windowCustomArray;
    private WindowCustom openedWindow;

    protected virtual void StartCustom()
    {
        windowCustomArray = GetComponentsInChildren<WindowCustom>(true);
    }

    public void OpenWindow(WindowCustom windowCustom)
    {
        windowCustom.gameObject.SetActive(true);
        windowCustom.Open();

        openedWindow = windowCustom;

        foreach (var window in windowCustomArray)
        {
            if (window != windowCustom)
                window.Close();
        }
    }

    public void ReturnWindow()
    {
        for (int i = windowCustomArray.Length - 1; i >= 0; i--)
        {
            if (windowCustomArray[i] == openedWindow && i > 0)
            {
                OpenWindow(windowCustomArray[i - 1]);
                break;
            }
        }
    }
}
