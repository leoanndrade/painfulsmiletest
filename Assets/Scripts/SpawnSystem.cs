using UnityEngine;

public class SpawnSystem : MonoBehaviour
{
    [SerializeField] private GameObject playerObj;
    [SerializeField] private GameObject[] enemyObjArray = new GameObject[] { };

    [SerializeField] private Transform[] spawnPointArray = new Transform[] { };

    public void SpawnPlayer()
    {
        if (GameManager.SavedPlayerTransform != null)
            return;

        GameManager.SavedPlayerTransform = Instantiate(playerObj).transform;
    }

    public void SpawnEnemy()
    {
        if (GameManager.SavedPlayerTransform != null)
        {
            float spawnChances = Random.Range(0, 101);
            GameObject enemyToInstantiate = enemyObjArray[spawnChances < 50 ? 0 : 1];

            Vector2 spawnPosition = spawnPointArray[Random.Range(0, spawnPointArray.Length)].position;
            
            Instantiate(enemyToInstantiate, spawnPosition, Quaternion.identity);
        }
    }
}
