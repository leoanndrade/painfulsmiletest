using UnityEngine;

public class PlayerShipManager : Ship
{
    private void Awake()
    {
        EventManager.OnDeleteAll += Delete;
        EventManager.OnPlayerShoot += Shoot;
    }

    private void Start()
    {
        healthStruct.isPlayer = true;
        ShipStartCustom();

        Camera.main.GetComponent<CameraController>().SetTarget = this.transform;
    }

    private void Delete()
    {
        Destroy(this.gameObject);
    }

    private void Update()
    {
        ShipUpdateCustom();

        Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        MovementBehaviour(input);

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Z))
        {
            TakeDamage(50);
        }
#endif
    }

    private void OnDestroy()
    {
        EventManager.OnDeleteAll -= Delete;
        EventManager.OnPlayerShoot -= Shoot;
    }
}