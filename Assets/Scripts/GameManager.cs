using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    private Transform playerTransform;
    public static Transform SavedPlayerTransform
    {
        get
        {
            return instance.playerTransform;
        }
        set
        {
            instance.playerTransform = value;
        }
    }

    public enum Scenes
    {
        MAIN_SCENE,
        GAME_SCENE
    }

    private GameSessionConfig _savedGameSessionConfig = new GameSessionConfig(100, 2);
    public GameSessionConfig SavedGameSessionConfig
    {
        get 
        { 
            return _savedGameSessionConfig; 
        }
        set { _savedGameSessionConfig = value; }
    }

    private void Awake()
    {
        GameManager[] gameManagerArray = FindObjectsOfType<GameManager>();
        if (gameManagerArray.Length > 1) 
        { 
            Destroy(this.gameObject); 
        }
        DontDestroyOnLoad(this.gameObject);
        
        instance = this;
    }

    public static void ChangeScene(Scenes scene)
    {
        switch (scene)
        {
            case Scenes.MAIN_SCENE:
                SceneManager.LoadScene("MainScene");
                break;
            case Scenes.GAME_SCENE:
                SceneManager.LoadScene("GameScene");
                break;
        }
    }
}