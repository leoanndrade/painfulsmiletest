using TMPro;
using UnityEngine;

public class HUDManager : Window
{
    [SerializeField] private WindowCustom mainWindow;
    [SerializeField] private WindowCustom endSessionWindow;
    [SerializeField] private TextMeshProUGUI txtSessionTimer;
    [SerializeField] private TextMeshProUGUI txtTotalPoint;
    [SerializeField] private GameObject healthLabelObj;

    private Transform myTransform;
    private Transform MyTransform
    {
        get
        {
            if(myTransform == null)
                myTransform = this.transform;

            return myTransform;
        }
    }

    private void Awake()
    {
        EventManager.OnDamageableObjInstantiate += DamageableObjInstantiate;
        EventManager.OnSessionEnd += SessionEnd;
        EventManager.OnSessionTimerChange += SessionTimerChange;
        EventManager.OnGivePlayerPoint += PlayerReceivePoint;
    }

    private void Start()
    {
        StartCustom();
    }

    private int playerPoint;
    private void PlayerReceivePoint(int point)
    {
        playerPoint += point;
        txtTotalPoint.text = "Total Points: " + playerPoint.ToString();
    }

    protected override void StartCustom()
    {
        base.StartCustom();
        OpenWindow(mainWindow);
        StartCoroutine(FadeTransition.Fade(FadeTransition.FadeType.IN));
    }

    public void MainMenuButton()
    {
        GameManager.ChangeScene(GameManager.Scenes.MAIN_SCENE);
    }

    public void PlayAgainButton()
    {
        GameManager.ChangeScene(GameManager.Scenes.GAME_SCENE);
    }

    public void PlayerShoot(int shotTypeIndex)
    {
        EventManager.PlayerShoot((ShotType)shotTypeIndex);
    }

    private void DamageableObjInstantiate(DamageableObject damageableObject)
    {
        HealthLabel healthLabel = Instantiate(healthLabelObj, MyTransform).GetComponent<HealthLabel>();
        int colorIndex = 0;
        if(!damageableObject.transform.root.GetComponent<PlayerShipManager>())
            colorIndex = 1;
        healthLabel.Initialize(damageableObject, colorIndex);
    }

    private void SessionEnd(bool playerWin)
    {
        OpenWindow(endSessionWindow);
    }

    private void SessionTimerChange(float time)
    {
        System.TimeSpan t = System.TimeSpan.FromSeconds(time);
        txtSessionTimer.text = string.Format("{0:D2}:{1:D2}", t.Minutes, t.Seconds);

        txtSessionTimer.text = time <= 0 ? "" : txtSessionTimer.text;
    }
    private void OnDestroy()
    {
        EventManager.OnDamageableObjInstantiate -= DamageableObjInstantiate;
        EventManager.OnSessionEnd -= SessionEnd;
        EventManager.OnSessionTimerChange -= SessionTimerChange;
        EventManager.OnGivePlayerPoint -= PlayerReceivePoint;
    }
}