using System.Collections.Generic;
using UnityEngine;

public class EnemyShipManager : Ship
{
    private enum EnemyType
    {
        SHOOTER,
        CHASER
    }

    [SerializeField] private EnemyType enemyType;
    [SerializeField] private float rotSpeed;

    private Vector2 finalInput;
    private float[] inputX = new float[] { 0, 0 };
    private Transform _myTransform;
    private Transform MyTransform
    {
        get
        {
            if (_myTransform == null)
                _myTransform = this.transform;

            return _myTransform;
        }
    }

    [SerializeField] private float visibilityRange;
    [SerializeField] private float terrainRaycastDist = 1;

    private int terrainLayerMask;
    private int terrainDetectionRayCount = 4;

    private int[] inputMinMax = new int[] { 0, 1 };

    private void Awake()
    {
        EventManager.OnDeleteAll += Delete;
        terrainLayerMask = LayerMask.GetMask("Terrain");
    }

    private void Start()
    {
        healthStruct.isPlayer = false;
        canFixShotSide = false;
        ShipStartCustom();
    }

    private void Update()
    {
        ShipUpdateCustom();

        if (healthStruct.health <= 0)
            return;

        FollowPlayer();
        TerrainAvoidance();

        if (enemyType == EnemyType.SHOOTER)
            ShootBehaviour();

        for (int i = 0; i < inputX.Length; i++)
        {
            finalInput.x += inputX[i];
        }
        finalInput.x /= inputX.Length;

        UpdatePlayerInfos();
        MovementBehaviour(finalInput);
    }

    private void Delete()
    {
        Destroy(this.gameObject);
    }

    float playerDist;
    Vector2 playerPos;
    Vector2 playerDir;
    private void UpdatePlayerInfos()
    {
        if (GameManager.SavedPlayerTransform != null)
        {   
            playerPos = GameManager.SavedPlayerTransform.position;
            playerDist = Vector2.Distance(MyTransform.position, playerPos);
            playerDir = new Vector2(MyTransform.position.x, MyTransform.position.y) - playerPos;
        }
    }

    private void ShootBehaviour()
    {
        if (playerDist <= visibilityRange)
        {
            float angle = GetAngle(playerDir);
            if (angle < 10)
            {
                Shoot(ShotType.FRONTAL_SINGLE);
            }
            else if (angle > 80 && angle < 100)
            {
                float angleDir = GetAngleDir(MyTransform.position, playerPos);
                
                if (angleDir < 0)
                    Shoot(ShotType.RIGHT_TRIPLE);
                else
                    Shoot(ShotType.LEFT_TRIPLE);
            }
        }
    }

    private void FollowPlayer()
    {
        float angleDir = GetAngleDir(MyTransform.position, new Vector2(MyTransform.position.x, MyTransform.position.y) + (-playerDir * terrainRaycastDist));

        inputX[0] = Mathf.Clamp(angleDir, -1.0f, 1.0f);

        inputX[0] = isAvoidingTerrain ? 0 : inputX[0];
    }

    bool isAvoidingTerrain = false;
    private void TerrainAvoidance()
    {
        List<Vector3> availableDir = new List<Vector3> { MyTransform.forward };
        RaycastHit2D terrainClosestHit = new RaycastHit2D();
        isAvoidingTerrain = false;
        for (int i = 0; i < terrainDetectionRayCount; i++)
        {
            Quaternion myRotation = MyTransform.rotation;
            Quaternion rotMod = Quaternion.AngleAxis((i / ((float)terrainDetectionRayCount - 1)) * 45 * 2 - 45, MyTransform.forward);
            Vector2 dir = myRotation * rotMod * Vector2.down;

            RaycastHit2D hit = Physics2D.Raycast(MyTransform.position, dir, terrainRaycastDist, terrainLayerMask);
            if (hit)
            {
                isAvoidingTerrain = true;
                if (terrainClosestHit.distance == 0)
                {
                    if (hit.distance < terrainClosestHit.distance)
                        terrainClosestHit = hit;
                }

                Debug.DrawRay(MyTransform.position, dir * terrainRaycastDist, Color.red);
            }
            else
            {
                availableDir.Add(-dir);
            }
        }

        Vector3 finalDir = new Vector3();
        for (int i = 0; i < availableDir.Count; i++)
        {
            finalDir += availableDir[i];
        }
        finalDir /= availableDir.Count;
        Debug.DrawRay(MyTransform.position, -finalDir * terrainRaycastDist, Color.green);
        if (finalDir == MyTransform.forward && terrainClosestHit.distance < terrainRaycastDist)
        {
            finalDir = MyTransform.right;

            finalInput.y = Mathf.Clamp(terrainClosestHit.distance / terrainRaycastDist, 0, 1);
        }
        else
            finalInput.y = 1;

        inputX[1] = GetAngleDir(MyTransform.position, MyTransform.position + (-finalDir * terrainRaycastDist));
    }

    private float GetAngle(Vector2 dir)
    {
        Quaternion hitRotation = Quaternion.LookRotation(Vector3.forward, dir);
        return Quaternion.Angle(MyTransform.rotation, hitRotation);
    }

    public float GetAngleDir(Vector2 posA, Vector2 posB)
    {
        var directionToEnemy = posA - posB;
        float angleRef = Vector2.Angle(MyTransform.up, Vector2.up);
        float angleDir = Vector3.Dot(directionToEnemy, MyTransform.right);
        angleDir = angleRef >= 180 ? -angleDir : angleDir;

        return angleDir;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (enemyType == EnemyType.SHOOTER)
            return;

        if (healthStruct.health <= 0)
            return;

        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<DamageableObject>().TakeDamage(10);
            TakeDamage(healthStruct.maxHealth);
        }
    }

    private void OnDestroy()
    {
        EventManager.OnDeleteAll -= Delete;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(this.transform.position, visibilityRange);
    }
#endif
}