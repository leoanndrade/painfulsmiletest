using System;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public static Action<DamageableObject> OnDamageableObjInstantiate;
    public static Action<HealthStruct> OnHealthChange;
    public static Action<ShotType> OnPlayerShoot;
    public static Action<bool> OnSessionEnd;
    public static Action<float> OnSessionTimerChange;
    public static Action<int> OnGivePlayerPoint;
    public static Action OnDeleteAll;

    public static void DamageableObjInstantiate(DamageableObject obj)
    {
        OnDamageableObjInstantiate?.Invoke(obj);
    }

    public static void HealthChange(HealthStruct healthStruct)
    {
        OnHealthChange?.Invoke(healthStruct);
    }

    public static void PlayerShoot(ShotType shotType)
    {
        OnPlayerShoot?.Invoke(shotType);
    }

    public static void SessionEnd(bool playerWin)
    {
        OnSessionEnd?.Invoke(playerWin);
    }

    public static void SessionTimerChange(float time)
    {
        OnSessionTimerChange?.Invoke(time);
    }

    public static void GivePlayerPoint(int point)
    {
        OnGivePlayerPoint?.Invoke(point); 
    }

    public static void DeleteAll()
    {
        OnDeleteAll?.Invoke();
    }
}