using UnityEngine;

public struct GameSessionConfig
{
    public float timeMax;
    public float enemySpawnRate;

    public GameSessionConfig(float timeMax = 100, float enemySpawnRate =  2)
    {
        this.timeMax = timeMax;
        this.enemySpawnRate = enemySpawnRate;
    }
}

public class GameSessionManager : MonoBehaviour
{
    private GameSessionConfig gameSessionConfig;

    private SpawnSystem spawnSystem;
    private float time;
    private bool sessionEnd = false;

    private const string spawnRepeating = "SpawnRepeating";

    private int playerPoint;

    private void Awake()
    {
        EventManager.OnHealthChange += HealthChange;
        EventManager.OnGivePlayerPoint += PlayerReceivePoint;
    }

    private void Start()
    {
        if (GameManager.instance is null)
            return;

        spawnSystem = this.GetComponentInChildren<SpawnSystem>();

        gameSessionConfig = GameManager.instance.SavedGameSessionConfig;

        time = gameSessionConfig.timeMax;

        InvokeRepeating(spawnRepeating, 0, gameSessionConfig.enemySpawnRate);

        spawnSystem.SpawnPlayer();
    }

    private int previousIntegetTime = 0; 
    private void Update()
    {
        time -= Time.deltaTime;

        int integerTime = Mathf.RoundToInt(time);
            if (integerTime != previousIntegetTime)
                EventManager.SessionTimerChange(integerTime);
        previousIntegetTime = integerTime;

        if (time <= 0 && !sessionEnd)
        {
            if (integerTime <= 0)
            {
                EndSession(true);
            }
        }
    }

    private void PlayerReceivePoint(int point)
    {
        playerPoint += point;
    }

    private void HealthChange(HealthStruct healthStruct)
    {
        if (healthStruct.isPlayer)
        {
            if (healthStruct.health <= 0 && !sessionEnd)
                EndSession(false);
        }
    }

    private void SpawnRepeating()
    {
        spawnSystem.SpawnEnemy();
    }

    public void EndSession(bool playerWin)
    {
        sessionEnd = true;
        EventManager.SessionEnd(playerWin);
        EventManager.DeleteAll();
    }

    private void OnDestroy()
    {
        EventManager.OnHealthChange -= HealthChange;
        EventManager.OnGivePlayerPoint -= PlayerReceivePoint;
    }
}