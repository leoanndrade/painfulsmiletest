using UnityEngine;

[System.Serializable]
public enum ShotType
{
    FRONTAL_SINGLE = 0,
    RIGHT_TRIPLE = 1,
    LEFT_TRIPLE = 2
}

[System.Serializable]
public class ShipConfig
{
    [Header("MOVEMENT")]
    [Range(0, 90)]
    public float movSpeed;
    [Range(0, 90)]
    public float rotSpeed;

    [Header("ARMORY")]
    [SerializeField]
    public Cannon[] cannonArray;

    [HideInInspector]
    public Transform transf;
}

public class Ship : DamageableObject
{
    [Header("--------- SHIP ---------")]
    [SerializeField] private ShipConfig config;

    private float rotSensibility = 1f;
    private float movSensibility = .04f;

    protected bool canFixShotSide = true;

    protected void ShipStartCustom()
    {
        config.transf = this.transform;

        DamageablObjectStartCustom();
    }

    protected void ShipUpdateCustom()
    {
        //Debug.Log(" eulerAngles: " + transform.eulerAngles +  "wrapAngle: " + WrapAngle(transform.eulerAngles.z));
    }

    float rotSpeedLerp = 0;
    float movSpeedLerp = 0;
    protected void MovementBehaviour(Vector2 input)
    {
        #region Rotation
        input = healthStruct.health <= 0 ? Vector2.zero : input;

        rotSpeedLerp = Mathf.Lerp(rotSpeedLerp, (config.rotSpeed * input.x * -1), 2 * Time.deltaTime);

        if (config.transf != null)
            config.transf.Rotate((new Vector3(0, 0, rotSpeedLerp) * rotSensibility) * Time.deltaTime, Space.World);
        else
            Debug.LogError("No Ship Transform Reference(RotBehaviour)");
        #endregion

        #region Movement
        movSpeedLerp = Mathf.Lerp(movSpeedLerp, (config.movSpeed * input.y * -1), 2 * Time.deltaTime);

        if (config.transf != null)
            config.transf.Translate((config.transf.up * movSpeedLerp * movSensibility) * Time.deltaTime, Space.World);
        else
            Debug.LogError("No Ship Transform Reference(MovBehaviour)");
        #endregion
    }

    protected void Shoot(ShotType shotType)
    {
        if (healthStruct.health <= 0) 
            return;

        shotType = FixShotSideByAngle(shotType);

        int index = (int)shotType;
        if (config.cannonArray.Length > index)
            config.cannonArray[(int)shotType].Shoot();
    }

    private ShotType FixShotSideByAngle(ShotType shotType)
    {
        if (!canFixShotSide)
            return shotType;

        float wrapedAngle = WrapAngle(config.transf.eulerAngles.z);

        switch (shotType)
        {
            case ShotType.FRONTAL_SINGLE:
                return shotType;
            case ShotType.RIGHT_TRIPLE:
                if (wrapedAngle >= -90 && wrapedAngle <= 90)
                    return shotType == ShotType.LEFT_TRIPLE ? ShotType.RIGHT_TRIPLE : shotType; 
                else
                    return shotType == ShotType.RIGHT_TRIPLE ? ShotType.LEFT_TRIPLE : shotType;
            case ShotType.LEFT_TRIPLE:
                if (wrapedAngle >= -90 && wrapedAngle <= 90)
                    return shotType == ShotType.RIGHT_TRIPLE ? ShotType.LEFT_TRIPLE : shotType;
                else
                    return shotType == ShotType.LEFT_TRIPLE ? ShotType.RIGHT_TRIPLE : shotType;
        }

        return shotType;
    }

    private float WrapAngle(float angle)
    {
        angle %= 360;
        if (angle > 180)
            return angle - 360;

        return angle;
    }
}