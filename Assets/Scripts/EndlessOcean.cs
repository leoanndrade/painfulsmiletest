using UnityEngine;
using UnityEngine.Tilemaps;

public class EndlessOcean : MonoBehaviour
{
    [SerializeField] private Vector2 speed;
    [SerializeField] private Vector2 tileAnchorLimit;

    private Vector2 speedStrength = new Vector2(1, 1);

    private Vector2 movDir = new Vector2(1, 1);

    private Tilemap tilemap;

    private Vector2 tileAnchor;

    private void Start()
    {
        tilemap = this.GetComponent<Tilemap>();
    }

    private void Update()
    {
        movDir.x *= MovDirMultiplier(tileAnchor.x, tileAnchorLimit.x, movDir.x);
        movDir.y *= MovDirMultiplier(tileAnchor.y, tileAnchorLimit.y, movDir.y);

        float speedStrengthX = CalcSpeedStrength(tileAnchor.x, tileAnchorLimit.x);
        float speedStrengthY = CalcSpeedStrength(tileAnchor.y, tileAnchorLimit.y);

        tileAnchor += new Vector2((speed.x * speedStrengthX) * movDir.x, (speed.y * speedStrengthY) * movDir.y) * Time.deltaTime;

        tilemap.tileAnchor = tileAnchor;
    }

    private int MovDirMultiplier(float anchor, float anchorLimit, float currentDir)
    {
        if (anchor < -anchorLimit && currentDir < 0 || anchor >= anchorLimit && currentDir > 0)
        {
            return -1;
        }

        return 1;
    }

    private float CalcSpeedStrength(float anchor, float anchorLimit)
    {
        float strength = Mathf.Clamp(Mathf.Abs((anchor / anchorLimit) - 1), .1f, 1f);

        return strength;
    }
}