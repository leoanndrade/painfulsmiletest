using UnityEngine;

public class DeteriorationSystem : MonoBehaviour
{
    [SerializeField] private SpriteRenderer hull;
    [SerializeField] private SpriteRenderer sail;
    [SerializeField] private SpriteRenderer nest;
    [SerializeField] private SpriteRenderer flag;

    [SerializeField] private Sprite[] spriteSail;
    [SerializeField] private Sprite[] spriteHull;

    [Header("SPRITE DEATH")]
    [SerializeField] private Sprite spriteSailDeath;
    [SerializeField] private Sprite spriteHullDeath;
    [SerializeField] private Sprite spriteFlagDeath;

    private GameObject rootObj;

    private void Awake()
    {
        rootObj = this.transform.root.gameObject;

        EventManager.OnHealthChange += HealthChange;
    }

    private void HealthChange(HealthStruct healthStruct)
    {
        if (healthStruct.obj == rootObj)
        {
            UpdateDeteriotarion(healthStruct.health / healthStruct.maxHealth);
        }
    }

    public void UpdateDeteriotarion(float healthPercentage)
    {
        if (healthPercentage != 0)
        {
            sail.sprite = spriteSail[FindIndex(spriteSail.Length - 1, (healthPercentage - .5f) / .5f)];
            hull.sprite = spriteHull[FindIndex(spriteHull.Length - 1, healthPercentage)];
        }
        else
        {
            sail.sprite = spriteSailDeath;
            hull.sprite = spriteHullDeath;
            nest.enabled = false;
            flag.sprite = spriteFlagDeath;
        }
    }

    private int FindIndex(int indexMax, float healthPercentage)
    {
        int index = Mathf.Clamp(Mathf.RoundToInt(indexMax * (Mathf.Abs(healthPercentage - 1))), 0, indexMax);

        return index;
    }

    private void OnDestroy()
    {
        EventManager.OnHealthChange -= HealthChange;
    }
}