using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct CustomSlider
{
    public Slider slider;
    public TextMeshProUGUI txt;
}

public class OptionsManager : MonoBehaviour
{
    [SerializeField] private CustomSlider sliderGameSessionTime;
    [SerializeField] private CustomSlider sliderGameEnemySpawnTime;

    private void OnEnable()
    {
        sliderGameSessionTime.slider.value = GameManager.instance.SavedGameSessionConfig.timeMax;
        sliderGameEnemySpawnTime.slider.value = GameManager.instance.SavedGameSessionConfig.enemySpawnRate;
    }

    public void OnSliderChange()
    {
        GameManager.instance.SavedGameSessionConfig = 
                            new GameSessionConfig(sliderGameSessionTime.slider.value,
                                                  sliderGameEnemySpawnTime.slider.value);

        sliderGameSessionTime.txt.text = (sliderGameSessionTime.slider.value / 60).ToString("F2") + " min";
        sliderGameEnemySpawnTime.txt.text = Mathf.RoundToInt(sliderGameEnemySpawnTime.slider.value) + " sec";
    }
}