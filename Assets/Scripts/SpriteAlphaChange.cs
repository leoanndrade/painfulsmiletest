using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SpriteAlphaChange
{
    private static float speedAnim = .5f;

    public static IEnumerator ChangeAlphaAnim(List<SpriteRenderer> spriteRendererList, float alphaTarget, float delay = 0)
    {
        yield return new WaitForSeconds(delay);

        Color color = spriteRendererList[0].color;

        while (color.a != alphaTarget)
        {
            color.a = Mathf.MoveTowards(color.a, alphaTarget, speedAnim * Time.deltaTime);
            foreach (SpriteRenderer spriteItem in spriteRendererList)
            {
                spriteItem.color = color;
            }
            yield return null;
        }
    }

    public static IEnumerator ChangeAlphaAnim(SpriteRenderer spriteRenderer, float alphaTarget, float delay = 0)
    {
        yield return new WaitForSeconds(delay);

        Color color = spriteRenderer.color;
        while (color.a != alphaTarget)
        {
            color.a = Mathf.MoveTowards(color.a, alphaTarget, speedAnim * Time.deltaTime);
            spriteRenderer.color = color;
            yield return null;
        }
    }
}