using System.Collections;
using UnityEngine;

public class WindowCustom : MonoBehaviour
{
    private CanvasGroup _cg;
    private CanvasGroup MyCanvasGroup
    {
        get
        {
            if(_cg == null)
                _cg = GetComponent<CanvasGroup>();
            return _cg;
        }
    }

    [SerializeField] private bool isEnabled = false;

    private GameObject _myObj;
    public GameObject MyObj
    {
        get
        {
            if (_myObj == null)
                _myObj = this.gameObject;

            return _myObj;
        }
    }

    private void Start()
    {
        if (isEnabled)
            Open();
    }

    public void Open()
    {
        MyCanvasGroup.alpha = 0;

        StopAllCoroutines();
        StartCoroutine(CgAlphaAnim(1));
    }

    public void Close()
    {
        if (!MyObj.activeInHierarchy)
            return;

        StopAllCoroutines();
        StartCoroutine(CgAlphaAnim(0));
    }

    private IEnumerator CgAlphaAnim(float target)
    {
        while(MyCanvasGroup.alpha != target)
        {
            MyCanvasGroup.alpha = Mathf.MoveTowards(MyCanvasGroup.alpha, target, 10 * Time.deltaTime);
            yield return null;
        }

        if (target == 0)
            MyObj.SetActive(false);
    }
}