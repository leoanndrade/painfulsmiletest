using System.Collections;
using UnityEngine;

public class FadeTransition : MonoBehaviour
{
    public static FadeTransition instance;

    public enum FadeType
    {
        IN = 0,
        OUT = 1
    }

    private CanvasGroup _myCanvasGroup;
    private CanvasGroup MyCanvasGroup
    {
        get
        {
            if (_myCanvasGroup == null)
                _myCanvasGroup = this.GetComponent<CanvasGroup>();
            return _myCanvasGroup;
        }
    }

    private void Awake()
    {
        FadeTransition[] fadeTransitionArray = FindObjectsOfType<FadeTransition>();
        if (fadeTransitionArray.Length > 1) { Destroy(this.gameObject); }
        DontDestroyOnLoad(this.gameObject);

        instance = this;
    }

    public static IEnumerator Fade(FadeType fadeType)
    {
        int target = (int)fadeType;

        if (instance != null && instance.MyCanvasGroup != null)
        {
            instance.MyCanvasGroup.interactable = target == 1;
            instance.MyCanvasGroup.blocksRaycasts = target == 1;

            instance.MyCanvasGroup.alpha = target == 0 ? 1 : 0;

            while (instance != null && instance.MyCanvasGroup.alpha != target)
            {
                instance.MyCanvasGroup.alpha = Mathf.MoveTowards(instance.MyCanvasGroup.alpha, target, Time.deltaTime);
                yield return null;
            }
        }
    }
}
