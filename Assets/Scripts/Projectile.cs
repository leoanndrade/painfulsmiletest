using UnityEngine;

[System.Serializable]
public struct ProjectileConfig
{
    [HideInInspector] public float speed;
    [SerializeField] private float damage;
    [SerializeField] private float lifeTimeMax;

    public float GetDamage { get { return damage; } }
    public float GetLifeTimeMax { get { return lifeTimeMax; } }
}
public class Projectile : MonoBehaviour
{
    [SerializeField] private ProjectileConfig config;

    private bool initialized = false;

    private Transform myTransform;

    private float lifeTime;

    private Vector3 dir;

    private GameObject spawnerObj;

    public void Initialize(GameObject spawnerObj, Vector3 dir, float speed)
    {
        if (!initialized)
        {
            config.speed = speed;
            this.dir = dir;
            lifeTime = 0;
            this.spawnerObj = spawnerObj;
            myTransform = this.transform;
            initialized = true;
        }
    }

    private void Update()
    {
        if (myTransform == null)
            return;

        myTransform.Translate(dir * config.speed * Time.deltaTime, Space.World);

        lifeTime += Time.deltaTime;

        if (lifeTime >= config.GetLifeTimeMax)
        {
            Destroyed();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == spawnerObj)
            return;

        if (spawnerObj != null)
        {
            DamageableObject damageableObject = collision.gameObject.GetComponent<DamageableObject>();

            if (damageableObject != null)
            {
                damageableObject.TakeDamage(config.GetDamage, spawnerObj.GetComponent<DamageableObject>());
            }
        }

        lifeTime = config.GetLifeTimeMax;
    }

    private void Destroyed()
    {
        myTransform = null;
        Destroy(this.gameObject);
    }
}