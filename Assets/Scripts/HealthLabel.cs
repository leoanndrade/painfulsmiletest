using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthLabel : MonoBehaviour
{
    [SerializeField] private Color[] colorArray;
    [SerializeField] private Slider slider;
    [SerializeField] private Slider sliderBackground;
    [SerializeField] private Image imgSlider;

    [SerializeField] private TextMeshProUGUI txtHealthPercentage;

    private GameObject damageableObjRef;
    private Transform damageableTransfRef;
    private RectTransform myRectTransform;

    private Camera myCamera;

    private void Awake()
    {
        myCamera = Camera.main;

        myRectTransform = this.GetComponent<RectTransform>();

        slider.value = 1;
        sliderBackground.value = 1;
        txtHealthPercentage.text = Mathf.RoundToInt(slider.value) * 100 + "%";

        EventManager.OnHealthChange += UpdateHealth;
    }

    public void Initialize(DamageableObject damageableObjRef, int colorIndex = 0)
    {
        if (colorArray.Length > colorIndex)
            imgSlider.color = colorArray[colorIndex];

        damageableTransfRef = damageableObjRef.transform;
        this.damageableObjRef = damageableObjRef.gameObject;
    }

    private void LateUpdate()
    {
        if (damageableTransfRef != null)
        {
            Vector2 viewportPosition = myCamera.WorldToViewportPoint(damageableTransfRef.position);
            myRectTransform.anchorMin = viewportPosition;
            myRectTransform.anchorMax = viewportPosition;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void UpdateHealth(HealthStruct healthStruct)
    {
        if (damageableObjRef is null)
            return;

        if (healthStruct.obj == damageableObjRef)
        {
            StopAllCoroutines();
            StartCoroutine(SliderAnim(healthStruct.health / healthStruct.maxHealth));
        }
    }

    private IEnumerator SliderAnim(float valueTarget)
    {
        sliderBackground.value = slider.value;

        while (slider.value != valueTarget)
        {
            slider.value = Mathf.MoveTowards(slider.value, valueTarget, Time.deltaTime);
            txtHealthPercentage.text = Mathf.RoundToInt(slider.value * 100) + "%";
            yield return null;
        }
        while (sliderBackground.value != valueTarget)
        {
            sliderBackground.value = Mathf.MoveTowards(sliderBackground.value, valueTarget, .5f * Time.deltaTime);
            yield return null;
        }
    }

    private void OnDestroy()
    {
        EventManager.OnHealthChange -= UpdateHealth;
    }
}